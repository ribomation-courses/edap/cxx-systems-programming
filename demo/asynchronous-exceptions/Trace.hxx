#pragma once

#include <iostream>
#include <string>

using namespace std;

/**
 *  Keeps track of function/object live-cycles
 */
class Trace {
    const short  STEP  = 3;
    const char   SPACE = ' ';
    const string name, enter, exit, tab;
    static int   level;

    void log(const string& action) {
        cout << tab << "[" << name << "] " << action << endl;
    }

public:
    Trace(const string& name)
            : name{name},
              enter{"ENTER"}, exit{"LEAVE"},
              tab{string(static_cast<unsigned long>(level * STEP), SPACE)} {
        ++level;
        log(enter);
    }

    Trace(const string& name, void* addr)
            : name{name + " @ " + to_string((unsigned long) addr)},
              enter{"CREATE"}, exit{"DISPOSE"},
              tab{string(static_cast<unsigned long>(level * STEP), SPACE)} {
        ++level;
        log(enter);
    }

    ~Trace() {
        --level;
        log(exit);
    }
};

