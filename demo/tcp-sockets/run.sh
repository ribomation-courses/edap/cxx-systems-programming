#!/usr/bin/env bash
set -e
set -x

mkdir -p build
cd build
cmake -G 'Unix Makefiles' ..
make
echo "RUN: ./build/tcp-client"
echo "RUN: ./build/tcp-server"
