#pragma once

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <streambuf>
#include <istream>
#include <ostream>
#include <cstring>
#include <cerrno>

namespace ribomation {
    namespace tcp {
        using namespace std;

        template<typename Char>
        class BasicSocketBuffer : public basic_streambuf<Char> {
        public:
            typedef Char                                             char_type;
            typedef std::basic_streambuf<char_type>                  buf_type;
            typedef std::basic_ostream<char_type>                    stream_type;
            typedef typename buf_type::int_type                      int_type;
            typedef typename std::basic_streambuf<Char>::traits_type traits_type;

        protected:
            static const int    char_size = sizeof(char_type);
            static const size_t SIZE      = 512;
            char_type           obuf[SIZE];
            char_type           ibuf[SIZE];
            int                 sockFD;

        public:
            BasicSocketBuffer() : sockFD(0) {
                buf_type::setp(obuf, obuf + (SIZE - 1));
                buf_type::setg(ibuf, ibuf, ibuf);
            }

            virtual ~BasicSocketBuffer() { sync(); }

            void set_socket(int fd) { BasicSocketBuffer::sockFD = fd; }

            int get_socket() { return sockFD; }

        protected:
            int output_buffer() {
                size_t num = buf_type::pptr() - buf_type::pbase();
                if (send(sockFD, reinterpret_cast<char*>(obuf), num * char_size, 0) != num)
                    return traits_type::eof();
                buf_type::pbump(static_cast<int>(-num));
                return static_cast<int>(num);
            }

            virtual int sync() override {
                if (output_buffer() == traits_type::eof())
                    return traits_type::eof();
                return 0;
            }

            virtual int_type overflow(int_type c) override {
                if (c != traits_type::eof()) {
                    *buf_type::pptr() = c;
                    buf_type::pbump(1);
                }

                if (output_buffer() == traits_type::eof())
                    return traits_type::eof();
                return c;
            }

            virtual int_type underflow() override {
                if (buf_type::gptr() < buf_type::egptr())
                    return *buf_type::gptr();

                ssize_t num = recv(sockFD, reinterpret_cast<char*>(ibuf), SIZE * char_size, 0);
                if (num <= 0)
                    return traits_type::eof();

                buf_type::setg(ibuf, ibuf, ibuf + num);
                return *buf_type::gptr();
            }
        };

        template<typename Char>
        class BasicSocketStream : public basic_iostream<Char> {
        public:
            typedef Char                         char_type;
            typedef basic_iostream<char_type>    stream_type;
            typedef BasicSocketBuffer<char_type> buf_type;

        protected:
            buf_type buf;

        public:
            BasicSocketStream() : stream_type(&buf) {}
            BasicSocketStream(int sockFD) : stream_type(&buf) {
                buf.set_socket(sockFD);
            }
            BasicSocketStream(const string& host, uint16_t port) : stream_type(&buf) {
                open(host, port);
            }
            ~BasicSocketStream() { close(); }

            void setFD(int fd) { buf.set_socket(fd); }

            void open(const string& host, uint16_t port) {
                close();
                hostent* ip = gethostbyname(host.c_str());
                sockaddr_in addr;
                ::memset(&addr, 0, sizeof(struct sockaddr_in));
                addr.sin_family = AF_INET;
                addr.sin_port   = htons(port);
                addr.sin_addr   = *(reinterpret_cast<struct in_addr*>(ip->h_addr));

                int sockFD = socket(AF_INET, SOCK_STREAM, 0);
                if (connect(sockFD, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
                    stream_type::setstate(ios::failbit);
                else
                    buf.set_socket(sockFD);

            }

            void close() {
                if (buf.get_socket() != 0) ::close(buf.get_socket());
                stream_type::clear();
            }
        };

        using SocketStream = BasicSocketStream<char>;


        class ServerSocket {
            int srvFD = -1;

        public:
            ServerSocket() = default;
            ServerSocket(int port) { open(port); }
            ~ServerSocket() { close(); };

            void open(int port) {
                struct sockaddr_in addr;
                ::memset(&addr, 0, sizeof(struct sockaddr_in));
                addr.sin_family      = AF_INET;
                addr.sin_port        = htons(port);
                addr.sin_addr.s_addr = htonl(INADDR_ANY);
                srvFD = socket(AF_INET, SOCK_STREAM, 0);
                bind(srvFD, reinterpret_cast<const struct sockaddr*>(&addr), sizeof(struct sockaddr_in));
                listen(srvFD, 5);
            }

            void close() {
                if (srvFD > -1) { ::close(srvFD); }
            }

            SocketStream& wait(SocketStream& client) {
                int clientFD = accept(srvFD, (struct sockaddr*) NULL, NULL);
                if (clientFD < 0) {
                    throw runtime_error(string("accept failed: ") + strerror(errno));
                }
                client.setFD(clientFD);
                return client;
            }
        };

    }
}

