#include <iostream>
#include <string>
#include <stdexcept>
#include <csignal>
#include <cstring>
#include <unistd.h>
#include <bits/types/siginfo_t.h>

using namespace std;

//void     (*sa_sigaction)(int, siginfo_t *, void *);
using handler = void (*)(int, siginfo_t*, void*);

void enableSignal(int signo, handler callback) {
    struct sigaction action{};
    action.sa_flags     = SA_SIGINFO;
    action.sa_sigaction = callback;
    if (sigaction(signo, &action, NULL) != 0)
        throw invalid_argument("sigaction: "s + strerror(errno));

    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, signo);
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) != 0)
        throw invalid_argument("sigprocmask: "s + strerror(errno));
}

using XXL = unsigned long long;

XXL fibonacci(unsigned n) {
    if (n == 0)return 0;
    if (n == 1)return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}


void compute(int signo, siginfo_t* info, void* ctx) {
    int arg = info->si_int;
    int pid = info->si_pid;
    cout << "RECV: arg=" << arg << ", pid=" << pid << endl;

    auto result = fibonacci(arg);
    cout << "fib(" << arg << ") = " << result << endl;
}


int main(int argc, char** argv) {
    enableSignal(SIGUSR1, &compute);
    cout << "Server waiting: PID=" << getpid() << endl;
    for (;;) pause();
}

//siginfo_t {
//int      si_signo;     /* Signal number */
//int      si_errno;     /* An errno value */
//int      si_code;      /* Signal code */
//int      si_trapno;    /* Trap number that caused
//                                         hardware-generated signal
//                                         (unused on most architectures) */
//pid_t    si_pid;       /* Sending process ID */
//uid_t    si_uid;       /* Real user ID of sending process */
//int      si_status;    /* Exit value or signal */
//clock_t  si_utime;     /* User time consumed */
//clock_t  si_stime;     /* System time consumed */
//sigval_t si_value;     /* Signal value */
//int      si_int;       /* POSIX.1b signal */
//void    *si_ptr;       /* POSIX.1b signal */
//int      si_overrun;   /* Timer overrun count;
//                                         POSIX.1b timers */
//int      si_timerid;   /* Timer ID; POSIX.1b timers */
//void    *si_addr;      /* Memory location which caused fault */
//long     si_band;      /* Band event (was int in
//                                         glibc 2.3.2 and earlier) */
//int      si_fd;        /* File descriptor */
//short    si_addr_lsb;  /* Least significant bit of address
//                                         (since Linux 2.6.32) */
//void    *si_lower;     /* Lower bound when address violation
//                                         occurred (since Linux 3.19) */
//void    *si_upper;     /* Upper bound when address violation
//                                         occurred (since Linux 3.19) */
//int      si_pkey;      /* Protection key on PTE that caused
//                                         fault (since Linux 4.6) */
//void    *si_call_addr; /* Address of system call instruction
//                                         (since Linux 3.5) */
//int      si_syscall;   /* Number of attempted system call
//                                         (since Linux 3.5) */
//unsigned int si_arch;  /* Architecture of attempted system call
//                                         (since Linux 3.5) */
//}
