#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

int main(int argc, char* argv[]) {
    long n = 10 * 1024 * 1024, s = 1024, k, sz = 0;
    if (argc > 1) n = atoi(argv[1]);

    printf("**** Allocating %ld blocks of size %ld bytes\n", n, s);
    for (k = 0; k < n; ++k) {
        void* blk = calloc(1, s);
        if (blk == NULL) {
            fprintf(stderr, "calloc() return NULL on %ldth invocation\n", k);
            fprintf(stderr, "Max HEAP: %.3f MB\n", sz / 1024.0);
            return 1;
        }
        sz++;;
    }
    printf("**** Got it all !!\n");
    fprintf(stderr, "Allocated: %.3f MB\n", sz / 1024.0);

    return 0;
}

