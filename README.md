Linux Systems Programming using Modern C++, 3 days
====


Welcome to this course.
The syllabus can be find at
[cxx/cxx-systems-programming](https://www.ribomation.se/cxx/cxx-systems-programming.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

Installation Instructions
====

The installation instructions and requirements of this course is the same as for all other C++ courses in this program.

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

