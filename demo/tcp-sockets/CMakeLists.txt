cmake_minimum_required(VERSION 3.5)
project(tcp_sockets)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmax-errors=1 -Wall -Wextra -Wno-sign-compare")

add_executable(tcp-client
  tcp-socket.hxx
  tcp-client.cxx
)
add_executable(tcp-server
  tcp-socket.hxx
  tcp-server.cxx
)
