//COMPILE: g++ --std=c++11 -Wall -g uptime.cpp -o my-uptime
//RUN    : ./my-uptime

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>

using namespace std;

string toTimeString(long time) {
    const unsigned long minute = 60;
    const unsigned long hour   = 60 * minute;
    const unsigned long day    = 24 * hour;
    ostringstream       buf;
    buf << (time % day) / hour;
    buf << ':' << (time % hour) / minute;
    buf << ':' << time % minute;
    return buf.str();
}

int numCPUs() {
    ifstream cpuinfo("/proc/cpuinfo");
    if (!cpuinfo) throw invalid_argument("Cannot open /proc/cpuinfo");

    int    cpuCount = 0;
    string line;
    while (getline(cpuinfo, line)) {
        if (line.find("processor") != string::npos) cpuCount++;
    }

    return cpuCount;
}

int main() {
    ifstream uptime("/proc/uptime");
    if (!uptime) throw invalid_argument("Cannot open /proc/uptime");

    double boot, idle;
    uptime >> boot >> idle;
    idle = idle / numCPUs();

    cout << "Time since boot: " << toTimeString(static_cast<long>(boot)) << endl;
    cout << "Idle time: " << toTimeString(static_cast<long>(idle)) << endl;

    return 0;
}
