#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "Process.hxx"

using namespace std;


class Kid : public Process {
    unsigned id;

public:
    explicit Kid(unsigned id) : id(id) {
        start();
    }

    unsigned int getId() const {
        return id;
    }

protected:
    int run() override {
        ostringstream buf;
        buf << "[kid-" << id << "] pid=" << getpid() << ", parent-pid=" << getppid() << "\n";
        cout << buf.str();
        return id;
    }
};


int main(int numArgs, char *args[]) {
    auto numKids = (numArgs == 1) ? 10 : stoi(args[1]);

    cout << "[parent] creating " << numKids << " child processes\n";
    vector<Kid *> kids;
    for (auto k = 1U; k <= numKids; ++k) {
        kids.push_back(new Kid(k));
    }

    for (auto k: kids) {
        int exitCode = k->join();
        cout << "[parent] got exit-code=" << exitCode
             << " from child-" << k->getId()
             << ", pid=" << k->getPid()
             << endl;
        delete k;
    }

    return 0;
}
