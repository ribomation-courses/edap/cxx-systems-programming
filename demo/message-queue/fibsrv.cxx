#include <iostream>
#include "MessageQueue.hxx"
using namespace std;

unsigned fib(unsigned n) {
    return n <= 2 ? 1 : fib(n - 2) + fib(n - 1);
}

int main() {
    MessageQueue<unsigned> fromClient{"fibsrv_in", true};
    MessageQueue<unsigned> toClient{"fibsrv_out", true};

    cout << "[fibsrv] started" << endl;
    unsigned n;
    do {
        fromClient >> n;
        if (n < 1) break;
        cout << "[fibsrv] fib(" << n << ")..." << endl;

        long result = fib(n);
        cout << "[fibsrv] fib(" << n << ") = " << result << endl;

        toClient << result;
    } while (n > 0);

    return 0;
}
