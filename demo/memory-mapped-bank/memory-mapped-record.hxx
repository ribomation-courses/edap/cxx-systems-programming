#include <iostream>
#include <string>
#include <stdexcept>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

using namespace std;

template<typename RecordType>
class MemoryMappedRecords {
    const string   filename;
    const unsigned numRecords;
    void* payload;
    size_t payloadSize;

    bool exists(const string& filename) {
        struct stat fileInfo;
        if (stat(filename.c_str(), &fileInfo) == 0) return true;
        if (errno == ENOENT) return false;

        throw runtime_error("Cannot stat '" + filename + "': " + strerror(errno));
    }

    size_t size(const string& filename) {
        struct stat fileInfo;
        if (stat(filename.c_str(), &fileInfo) == 0) return static_cast<size_t>(fileInfo.st_size);
        throw runtime_error("Cannot stat '" + filename + "': " + strerror(errno));
    }

    void create(const string& filename, size_t size) {
        int fd = creat(filename.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (fd < 0) throw invalid_argument("failed to create '" + filename + "': " + strerror(errno));

        if (ftruncate(fd, size) < 0) throw invalid_argument(strerror(errno));
        close(fd);
    }

    void map(const string& filename, size_t size) {
        int fd = open(filename.c_str(), O_RDWR);
        if (fd < 0) throw invalid_argument("cannot open '" + filename + "': " + strerror(errno));

        payloadSize = size;
        payload     = (char*) mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (payload == MAP_FAILED) throw runtime_error(string("failed to mmap: ") + strerror(errno));

        close(fd);
    }

public:
    constexpr static size_t RECORD_SIZE = sizeof(RecordType);

    /**
     * Opens an <em>existing</em> record-oriented file and maps it into a memory segment
     * @param filename
     * @throws invalid_argument     if file is not found
     */
    MemoryMappedRecords(const string& filename) 
      : filename{filename}, numRecords{static_cast<unsigned>(size(filename) / RECORD_SIZE)} 
    {
        if (!exists(filename)) throw invalid_argument(filename + " not found");
        map(filename, numRecords * RECORD_SIZE);
    }

    /**
     * Creates a new record-oriented file of the given size
     * @param filename
     * @param numRecords
     * @throws invalid_argument     if file was found
     */
    MemoryMappedRecords(const string& filename, unsigned numRecords) : filename(filename), numRecords{numRecords} {
        if (exists(filename)) throw invalid_argument(filename + " already exists");
        create(filename, numRecords * RECORD_SIZE);
        map(filename, numRecords * RECORD_SIZE);
    }

    /**
     * Disposes the memory segment
     */
    virtual ~MemoryMappedRecords() {
        munmap(payload, payloadSize);
    }

    /**
     * Write the segment back to disk
     */
    void update(bool sync = false) {
        msync(payload, payloadSize, sync ? MS_SYNC : MS_ASYNC);
    }

    /**
     * Size of the segment in bytes
     * @return
     */
    size_t bytes() const {
        return payloadSize;
    }

    /**
     * Size of the segment in number of records
     * @return
     */
    unsigned records() const {
        return numRecords;
    }

    /**
     * Returns the segment as an array of records
     * @return
     */
    RecordType* array() {
        return reinterpret_cast<RecordType*>(payload);
    }

    MemoryMappedRecords() = delete;
    MemoryMappedRecords(const MemoryMappedRecords<RecordType>&) = delete;
    MemoryMappedRecords<RecordType>& operator=(const MemoryMappedRecords<RecordType>&) = delete;
};

