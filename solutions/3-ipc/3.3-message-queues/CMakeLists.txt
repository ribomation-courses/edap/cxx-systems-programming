cmake_minimum_required(VERSION 3.9)
project(3_message_queues)

set(CMAKE_C_COMPILER_VERSION 99)
set(CMAKE_CXX_COMPILER_VERSION 17)
add_compile_options(-Wall -Wextra -Wfatal-errors)

add_executable(fibsrv intq.h intq.c fibsrv.c)
target_link_libraries(fibsrv rt)

add_executable(fibcli intq.h intq.c fibcli.c)
target_link_libraries(fibcli rt)

set(MQSRC echo.hxx message-queue.hxx)

add_executable(echo-server ${MQSRC} echo-server.cxx)
target_link_libraries(echo-server rt)

add_executable(echo-client ${MQSRC} echo-client.cxx)
target_link_libraries(echo-client rt)
