cmake_minimum_required(VERSION 3.8)
project(tiny_shell)

set(CMAKE_CXX_STANDARD 14)
add_compile_options(-Wall -Wextra -Wpedantic -Wfatal-errors)

add_executable(tiny_shell tiny-shell.cxx)