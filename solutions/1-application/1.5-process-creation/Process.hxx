#pragma once

#include <stdexcept>
#include <unistd.h>
#include <wait.h>
extern void exit(int status);

using namespace std;


class Process {
    pid_t pid;

public:
    Process() = default;
    virtual ~Process() = default;
    Process(const Process&) = delete;
    Process& operator=(const Process&) = delete;

    pid_t getPid() const {
        return pid;
    }

    void start() {
        pid = fork();
        if (pid < 0) {
            throw runtime_error("cannot fork");
        } else if (pid == 0) {
            int exitCode = run();
            exit(exitCode);
        }
    }

    int join() {
        int status;
        waitpid(pid, &status, 0);
        return WEXITSTATUS(status);
    }

protected:
    virtual int run() = 0;
};
