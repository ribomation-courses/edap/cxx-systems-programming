//COMPILE: c99 -g -Wall -D_GNU_SOURCE phrase-count.c -o phrase-count
//RUN    : ./phrase-count <phrase> <file>

#define _GNU_SOURCE
#include <stdlib.h> 
#include <stdio.h> 
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

static char*  memSegment;
static size_t memSegmentSize;

typedef char* (*SearchFunction)(const char*, const char*);
SearchFunction  search[] = {strstr, strcasestr};
const int respectCase = 0;
const int ignoreCase  = 1;
int       caseMode    = 0;
int       verbose     = 0;

void  error(char* msg) {
  printf("error: %s\n", msg); 
  perror("phrase-count");
  exit(1);
}

size_t  fileSize(const char* filename) {
  struct stat  fileInfo;
  if (stat(filename, &fileInfo) < 0) error("cannot stat file");
  return fileInfo.st_size;
}

void cleanup() {
	if (verbose) printf("[cleanup] Unmapping the segment\n");
	munmap(memSegment, memSegmentSize);
}

char*  loadFile(const char* filename, const size_t fileSz) {
  if (verbose) printf("[loadFile] Loading file '%s' ...\n", filename);
  int  fd = open(filename, O_RDONLY);
  if (fd < 0) error("Cannot open file");

  char* contents = mmap(0, fileSz, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
  if (contents == MAP_FAILED) error("Failed to map file");
  close(fd);  //no need for the FD any longer
  
  // Close the string.
  contents[fileSz-1] = '\0';
  // Because we "modify" the content here, we need PROT_WRITE above.
  // This implies that we don't care of the very last character ;-)


  if (verbose) 
    printf("[loadFile] Mapped file '%s' to addr %p with size %ld bytes\n", 
	        filename, contents, (long int)fileSz);
	
  memSegment     = contents;
  memSegmentSize = fileSz;
  atexit(cleanup);
	
  return contents;
}

int doCount(const char* phrase, const char* begin, const char* end) {
  int  count = 0, phraseSz = strlen(phrase);
  while (begin != end) {
    char* p = (*search[caseMode])(begin, phrase);
    if (!p) break;
    count++;
    begin = p + phraseSz;
  }
  return count;
}

int main(int argc, char* argv[]) {
  char*  filename = "./shakespeare.txt";
  char*  phrase   = "Hamlet";

  for (int k=1; k<argc; ++k) {
    char* arg = argv[k];
    
    if (strcmp(arg, "-i")==0) {
        caseMode = ignoreCase;
    } else if (strcmp(arg, "-v")==0) {
        verbose = 1;
    } else if (strcmp(arg, "-q")==0) {
        verbose = 0;
    } else if (strcmp(arg, "-p")==0) {
        phrase = argv[++k];
    } else if (strcmp(arg, "-f")==0) {
        filename = argv[++k];
    } else {
        fprintf(stderr, "usage: %s [-i] [-p <phrase>] [-f <file>]\n", argv[0]);
        exit(1);
    }
  }
  
  size_t  fileSz      = fileSize(filename) + 1;
  char*   contents    = loadFile(filename, fileSz);
  int     phraseCount = doCount(phrase, contents, contents + fileSz);
  printf("Found %d occurences of '%s'\n", phraseCount, phrase);
  
  return 0;
}
