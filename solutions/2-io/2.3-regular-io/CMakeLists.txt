cmake_minimum_required(VERSION 3.8)
project(09_basic_io)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -fmax-errors=1 -Wall -Wextra -Wno-sign-compare")

add_executable(logger logger.c)
