cmake_minimum_required(VERSION 3.5)
project(memory_mapped_bank)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -fmax-errors=1 -Wall -Wextra -Wno-pointer-arith -Wno-unused-parameter ")

add_executable(create
  account.hxx
  memory-mapped-record.hxx
  create.cxx
)

add_executable(print
  account.hxx
  memory-mapped-record.hxx
  print.cxx
)

add_executable(update
  account.hxx
  memory-mapped-record.hxx
  update.cxx
)
