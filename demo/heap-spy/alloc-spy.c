#include <stdio.h>
#include <malloc.h>

int main() {
    int numBlocks = 1000, blockSize = 1000;

    printf("**** Allocating %d small blocks\n", numBlocks);
    for (int k = 0; k < numBlocks; ++k) {
        void* small = calloc(1, blockSize);
    }

    printf("**** Allocating 1 large block\n");
    void* large = calloc(numBlocks, blockSize);

    // ** intentionally skipping free()
    return 0;
}

