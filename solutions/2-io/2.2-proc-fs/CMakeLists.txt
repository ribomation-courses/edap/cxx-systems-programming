cmake_minimum_required(VERSION 3.6)
project(2_proc_fs)

set(CMAKE_CXX_COMPILER_VERSION 17)
add_compile_options(-Wall -Wextra -Wfatal-errors)

add_executable(uptime uptime.cxx)
