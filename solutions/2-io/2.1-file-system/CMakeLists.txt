cmake_minimum_required(VERSION 3.5)
project(08_file_systems)


set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -fmax-errors=1 -Wall -Wextra -D_DEFAULT_SOURCE")
add_executable(myls my-ls.c)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Wfatal-errors -D_DEFAULT_SOURCE )
add_executable(lsxx my-ls.cxx)
