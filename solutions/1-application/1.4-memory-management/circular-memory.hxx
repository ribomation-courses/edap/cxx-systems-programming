#pragma  once

#include <stdexcept>

namespace ribomation {
    namespace memory {
        using namespace std;
        using namespace std::string_literals;
        using byte = unsigned char;

        template<unsigned capacity>
        class CircularMemory {
            constexpr static unsigned CAPACITY       = capacity;
            constexpr static unsigned MAX_BLOCK_SIZE = CAPACITY / 10;

            byte storage[CAPACITY];
            byte* nextAddress = storage;

        public:
            CircularMemory() = default;
            ~CircularMemory() = default;
            CircularMemory(const CircularMemory&) = delete;
            CircularMemory& operator=(const CircularMemory&) = delete;

            byte* alloc(unsigned size) {
                if (size > MAX_BLOCK_SIZE) {
                    throw overflow_error("too large block size"s);
                }

                byte* addr = nextAddress;
                nextAddress += size;

                if (nextAddress > (storage + CAPACITY)) {
                    nextAddress = storage;
                    addr        = nextAddress;
                    nextAddress += size;
                }

                return addr;
            }
        };
    }
}
