
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <pwd.h>

extern void exit(int);

void child() {
    struct passwd* pass = getpwuid(getuid());
    char* username = pass->pw_name;
    if (username == NULL) {
        perror("child");
        exit(42);
    }

    printf("[child] username=%s\n", username);
    execl("/bin/ps", "ps", "-ejH", "-u", username, NULL);
    exit(1);
}

int main() {
    printf("[parent] before fork\n");
    if (fork() == 0) {
        child();
    }

    printf("[parent] waiting for child termination\n");
    int status = 0;
    wait(&status);
    printf("[parent] child exit-code=%d\n", WEXITSTATUS(status));

    return 0;
}
