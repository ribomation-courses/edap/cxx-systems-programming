#pragma once

#include <sstream>
#include <string>
#include <pthread.h>

namespace {
    using namespace std;

    class Thread {
        const string    name;
        pthread_t thrId;

        static void* runWrapper(void* arg) {
            Thread* self = (Thread*) arg;
            self->run();
            return nullptr;
        }

    protected:
        virtual void run() = 0;

        const string& getName() const {
            return name;
        }

        void log(const string& what) {
            ostringstream buf;
            buf << name << what << "\n";
            cout << buf.str();
        }

    public:
        Thread(const string& name) : name("[" + name + "] ") { }

        virtual ~Thread() = default;

        virtual void start() final {
            pthread_create(&thrId, NULL, runWrapper, this);
        }

        virtual void join() final {
            pthread_join(thrId, NULL);
        }

        Thread()              = delete;
        Thread(const Thread&) = delete;
        Thread& operator=(const Thread&) = delete;
    };

}
