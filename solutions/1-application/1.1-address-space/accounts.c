#include <stdio.h>
#include <stdlib.h>

// -------------------------
// --- Account
// -------------------------
typedef struct {
    int     balance;
    float   rate;
} Account;

Account*  account_init(Account* this, int balance, float rate) {
    this->balance = balance;
    this->rate    = rate;
    return this;
}

void account_print(Account* this) {
    printf("Account{SEK %d, %.1f%%} @ %p\n",
        this->balance, this->rate, (void*)this);
}


// -------------------------
// --- App
// -------------------------
Account  global;

void func() {
    printf(">> func()\n");
    Account     local;
    account_init(&local, 400, 5);
    printf("STACK: "); account_print(&local);
    printf("<< func()\n");
}

int main() {
    Account     local;
    Account*    heap = (Account*)calloc(1, sizeof(Account));

    account_init(&global, 100, 2);
    account_init(&local, 200, 3);
    account_init(heap  , 300, 4);

    printf("DATA : "); account_print(&global);
    printf("STACK: "); account_print(&local);
    printf("HEAP : "); account_print(heap);

    func();

    free(heap);

    return 0;
}
