#include <iostream>
#include <string>
#include <stdexcept>
#include <csignal>

using namespace std;

int main(int argc, char** argv) {
    if (argc != 3) {
        throw invalid_argument("options: <pid> <arg>");
    }
    pid_t pid = stoi(argv[1]);
    int   arg = stoi(argv[2]);

    sigval payload{};
    payload.sival_int = arg;
    sigqueue(pid, SIGUSR1, payload);
    cout << "Sent: " << arg << " to " << pid << endl;

    return 0;
}
