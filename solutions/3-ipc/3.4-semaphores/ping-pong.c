#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PROCESS_SHARED     1

void error(char* msg) { perror(msg); exit(2); }

void*   newSHM(size_t size, const char* name) {
    int     fd = shm_open(name, O_CREAT | O_TRUNC | O_RDWR, 0666);
    if (fd < 0) error("Cannot create SHM");
    
    int     shmSize = getpagesize() * (1 + size % getpagesize()); //multiple of pages
    if (ftruncate(fd, shmSize) < 0) error("Cannot set sizse of SHM");

    void*   shm = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm == MAP_FAILED) error("Cannot mmap SHM");
    close(fd);    
    memset(shm, 0, size);

    return shm;
}

void    destroySHM(const char* name) { shm_unlink(name); }

sem_t*  newSemaphore(int count, void** storage) {
    sem_t   semaphore;
    sem_t*  result = (sem_t*)memcpy(*storage, &semaphore, sizeof(sem_t));
    *storage += sizeof(sem_t);
    
    if (sem_init(result, PROCESS_SHARED, count) < 0) error("Cannot create semaphore");
    
    return result;
}

void    destroySEM(sem_t* sem) { sem_destroy(sem); }

void    task(int numOperations, int* cnt, const char* msg, sem_t* my_turn, sem_t* your_turn) {
    int  k; for (k = 0; k < numOperations; ++k) {
        if (sem_wait(my_turn) != 0) error("Failed to invoke sem_wait");
            
        printf("%s (%d)\n", msg, *cnt); 
        *cnt = *cnt + 1;
        
        if (sem_post(your_turn) != 0) error("Failed to invoke sem_post");
    }
}

int main(int argc, char* argv[]) {
    int         n = 25; if (argc > 1) n = atoi(argv[1]);
    const char* shmName = "/ping-pong";
    size_t      shmSize = 2 * sizeof(sem_t) + 1 * sizeof(int);
    
    printf("Creating SHM\n");
    void*   shm = newSHM(shmSize, shmName);
    void*   storage = shm;
    printf("Creating SEM 1\n");
    sem_t*  sem1 = newSemaphore(1, &storage);

    printf("Creating SEM 2\n");
    sem_t*  sem2 = newSemaphore(0, &storage);
    
    int*    theCount = (int*)storage; storage += sizeof(int);
    *theCount = 1;

    if (fork() == 0) {
        printf("PONG started\n");
        task(n, theCount, "            PONG", sem2, sem1);
        exit(0);
    }
    
    printf("PING started\n");
    task(n, theCount, "PING", sem1, sem2);   
    destroySEM(sem2); 
    destroySEM(sem1); 
    destroySHM(shmName);

    return 0;
}

