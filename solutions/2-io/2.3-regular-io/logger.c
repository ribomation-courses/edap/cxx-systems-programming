//COMPILE: c99 -g -Wall logger.c -o logger
//RUN    : ./logger <num-writes> <message> <logfile>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int numArgs, char* args[]) {
  int    numMsgs  = 50;
  char*  msg      = "Linux is Cool";
  char*  filename = "./logger.log";

  if (numArgs > 1) numMsgs  = atoi( args[1] );
  if (numArgs > 2) msg      = args[2];
  if (numArgs > 3) filename = args[3];
  
  const int MAXLINE = 1024;
  if (strlen(msg) > (MAXLINE-2)) {
    fprintf(stderr, "Too long message\n");
    exit(1);
  }

  int  log = open(filename, 
                  O_WRONLY | O_CREAT | O_APPEND, 
                  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);  //rw-r--r--
  if (log < 0) {perror("open"); exit(1);}  
  printf("log file '%s' opened in append mode\n", filename);
  
  char logline[MAXLINE];
  //strncpy(logline, msg, MAXLINE); 
  //strncat(logline, "\n", MAXLINE);
  //const int linesize = strlen(logline);

  size_t numBytes = 0;
  int    numRemaining = numMsgs;
  do {
    //time_t      now     = time(NULL);
    struct timeval  now;
    gettimeofday(&now, NULL);
    
    struct tm*  nowData = localtime(&now.tv_sec);
    size_t n = strftime(logline, sizeof(logline), "%F %T", nowData);
    
    sprintf(logline+n, " (%d) :: %s\n", (int)(now.tv_usec % (int)1E6), msg);
    
    numBytes += write(log, logline, strlen(logline));
  } while (numRemaining-- > 0);
  
  close(log);

  printf("--- %s ---\n  %d messages\n  %d bytes/message\n  %d bytes total\n",
		  filename, numMsgs, (int)strlen(msg), (int)numBytes);
		  
  return 0;
}

