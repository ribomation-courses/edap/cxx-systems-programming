
#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <algorithm>
#include <numeric>
#include <locale>
#include "account.hxx"
#include "memory-mapped-record.hxx"

using namespace std;

class Random {
    random_device                     e;
    string                            symbols;
    uniform_int_distribution<size_t>  sumIdx;
    normal_distribution<double>       balance{100, 75};
    uniform_real_distribution<double> rate{0.25, 3.5};

public:
    Random() {
        for (auto                        k = 'A'; k <= 'Z'; ++k) symbols += k;
        for (auto                        k = '0'; k <= '9'; ++k) symbols += k;
        uniform_int_distribution<size_t> d{0, symbols.size() - 1};
        sumIdx = d;
    }

    char nextSymbol() {
        return symbols[sumIdx(e)];
    }

    string nextIBAN() {
        //CZ34 3054 6101 5659 0995 1803
        string    iban = "";
        for (auto k    = 0; k < 6; ++k) {
            if (k > 0) iban += " ";
            iban += nextSymbol();
            iban += nextSymbol();
            iban += nextSymbol();
            iban += nextSymbol();
        }
        return iban;
    }

    long nextBalance() {
        return static_cast<long>(balance(e));
    }

    double nextRate() {
        return rate(e);
    }
};

Random r;


long sumBalances(const Account* accounts, unsigned int numRecords) {
    vector<long> balances;
    transform(accounts, accounts + numRecords, back_inserter(balances), [&](const Account& a) {
        return a.getBalance();
    });
    return accumulate(balances.begin(), balances.end(), 0);
}

size_t size(const string& filename) {
    struct stat fileInfo;
    if (stat(filename.c_str(), &fileInfo) == 0) return static_cast<size_t>(fileInfo.st_size);

    throw runtime_error("Cannot stat '" + filename + "': " + strerror(errno));
}

int main(int numArgs, char* args[]) {
    string   filename   = "./bank.db";
    unsigned numRecords = 10;
    bool     verbose    = true;

    for (auto k = 1; k < numArgs; ++k) {
        const string arg = args[k];
        if (arg == "-f") {
            filename = args[++k];
        }
        if (arg == "-n") {
            numRecords = static_cast<unsigned>(stoi(args[++k]));
        }
        if (arg == "-q") {
            verbose = false;
        }
        if (arg == "-v") {
            verbose = true;
        }
    }

    //cout.imbue(std::locale("sv_SE.UTF8"));
    cout << "File: " << filename << endl;
    cout << "# records: " << numRecords << endl;
    unlink(filename.c_str());

    {
        MemoryMappedRecords<Account> db{filename, numRecords};
        Account* accounts = db.array();

        for (auto k = 0U; k < numRecords; ++k) {
            accounts[k].setIban(r.nextIBAN());
            accounts[k].setBalance(r.nextBalance());
            accounts[k].setRate(r.nextRate());
            if (verbose) cout << "[" << k << "]: " << accounts[k] << endl;
        }

        cout << "Total balance = SEK " << sumBalances(accounts, numRecords) << endl;
    }

    cout << "Written " << filename << " ("
         << fixed << setprecision(3) << size(filename) / (1024.0 * 1024.0) << " MB)"
         << endl;


    return 0;
}
