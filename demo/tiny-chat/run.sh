#!/usr/bin/env bash
set -e
set -x

mkdir -p build
cd build
cmake -G 'Unix Makefiles' ..
make
echo "RUN: ./build/tiny-chat -s"
echo "RUN: ./build/tiny-chat -c"
